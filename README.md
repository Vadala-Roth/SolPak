An open source modular battery pack system based on the technology seen in
the Tesla Model 3 battery teardown. SolPak can be used for custom DIY EVs, 
home solar storage, home backup power, off the grid living,
portable battery pack, and camping.

 0  

https://gitlab.com/Vadala-Roth/SolPak.git
